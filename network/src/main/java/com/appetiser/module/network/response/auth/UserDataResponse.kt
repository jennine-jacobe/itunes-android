package com.appetiser.module.network.response.auth

import com.appetiser.module.network.models.APIUser

open class UserDataResponse(
    val user: APIUser,
    val token: String = ""
)
