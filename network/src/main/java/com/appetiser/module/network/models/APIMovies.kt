package com.appetiser.module.network.models

import com.google.gson.annotations.SerializedName

open class APIMovies(
    @SerializedName("trackId")
    open val trackId: Int = 0,
    @SerializedName("trackName")
    open val trackName: String = "",
    @SerializedName("primaryGenreName")
    open val primaryGenreName: String = "",
    @SerializedName("releaseDate")
    open val releaseDate: String = "",
    @SerializedName("trackTimeMillis")
    open val trackTimeMillis: Long = 0,
    @SerializedName("currency")
    open val currency: String = "",
    @SerializedName("trackPrice")
    open val trackPrice: Double = 0.0,
    @SerializedName("longDescription")
    open val longDescription: String = "",
    @SerializedName("artworkUrl100")
    open val artworkUrl100: String = ""
)
