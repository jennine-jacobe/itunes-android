package com.appetiser.module.network.response.movies

import com.appetiser.module.network.models.APIMovies
import com.google.gson.annotations.SerializedName

data class MoviesSearchResponse(
    @SerializedName("resultCount")
    val resultCount: Int = 0,
    @SerializedName("results")
    val results: List<APIMovies> = listOf()
)
