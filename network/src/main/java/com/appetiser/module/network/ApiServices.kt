package com.appetiser.module.network

import com.appetiser.module.network.response.movies.MoviesSearchResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface ApiServices {
    /**
     *
     * Movie API
     *
     **/
    @GET
    fun getMovies(
        @Url url: String,
        @Query("term") term: String = "star",
        @Query("country") country: String = "au",
        @Query("media") media: String = "movie"
    ): Single<MoviesSearchResponse>
}
