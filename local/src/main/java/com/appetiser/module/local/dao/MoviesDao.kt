package com.appetiser.module.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.appetiser.module.local.model.DBMovies
import io.reactivex.Single

@Dao
abstract class MoviesDao : BaseDao<DBMovies> {

    @Query("SELECT * FROM ${DBMovies.MOVIES_TABLE_NAME}")
    abstract fun getAllItunes(): Single<List<DBMovies>>

    @Query("SELECT COUNT(*) FROM ${DBMovies.MOVIES_TABLE_NAME}")
    abstract fun getCount(): Int

    @Query("DELETE FROM ${DBMovies.MOVIES_TABLE_NAME}")
    abstract fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(users: List<DBMovies>): LongArray

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(user: DBMovies): Long
}
