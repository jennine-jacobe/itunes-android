package com.appetiser.module.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = DBMovies.MOVIES_TABLE_NAME)
data class DBMovies(
    @PrimaryKey
    var trackId: Int = 0,
    var trackName: String = "",
    var primaryGenreName: String = "",
    var releaseDate: String = "",
    var trackTimeMillis: Long = 0,
    var currency: String = "",
    var trackPrice: Double = 0.0,
    var longDescription: String = "",
    var artworkUrl100: String = ""
) {
    companion object {
        const val MOVIES_TABLE_NAME = "itunes_movies"
    }
}
