package com.appetiser.module.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.appetiser.module.local.dao.MoviesDao
import com.appetiser.module.local.dao.TokenDao
import com.appetiser.module.local.dao.UserDao
import com.appetiser.module.local.model.DBMovies
import com.appetiser.module.local.model.DBToken
import com.appetiser.module.local.model.DBUserSession

@Database(
    entities = [
        DBUserSession::class,
        DBToken::class,
        DBMovies::class
    ],
    version = 1,
    exportSchema = true
)
@TypeConverters
abstract class AppDatabase : RoomDatabase() {

    abstract fun userSessionDao(): UserDao

    abstract fun tokenDao(): TokenDao

    abstract fun moviesDao(): MoviesDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                        ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context, AppDatabase::class.java, "itunes.db")
                .fallbackToDestructiveMigration()
                .build()
    }
}
