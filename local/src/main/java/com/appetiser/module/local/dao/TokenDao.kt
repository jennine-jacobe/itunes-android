package com.appetiser.module.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.appetiser.module.local.model.DBToken
import io.reactivex.Single

@Dao
abstract class TokenDao : BaseDao<DBToken> {

    @Query("SELECT * FROM ${DBToken.TOKEN_TABLE_NAME} LIMIT 1")
    abstract fun getToken(): Single<DBToken>

    @Query("DELETE FROM ${DBToken.TOKEN_TABLE_NAME}")
    abstract fun logoutToken()
}
