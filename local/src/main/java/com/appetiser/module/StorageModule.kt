package com.appetiser.module

import android.app.Application
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.appetiser.module.local.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Singleton
    fun providesSharedPreferences(application: Application): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(application.applicationContext)
    }

    @Provides
    @Singleton
    fun providesAppDatabase(application: Application): AppDatabase {
        return AppDatabase.getInstance(application.applicationContext)
    }
}
