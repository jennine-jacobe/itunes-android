package com.appetiser.module.data.mapper

import com.appetiser.module.local.model.DBToken
import com.appetiser.module.local.model.DBUserSession
import com.appetiser.module.data.poko.AccessToken
import com.appetiser.module.data.poko.UserSession
import com.appetiser.module.network.response.auth.AuthDataResponse

class UserSessionMapper private constructor() {

    fun mapFromDomain(from: AuthDataResponse): Map<String, Any> {
        val map = mutableMapOf<String, Any>()
        val user = from.data.user

        map[USER_KEY] = UserSession(
                fullName = user.fullName ?: "",
                firstName = user.firstName ?: "",
                lastName = user.lastName ?: "",
                email = user.email,
                photoUrl = user.photoUrl.orEmpty(),
                uid = user.uid,
                dateOfBirth = user.dateOfBirth.orEmpty(),
                emailVerifiedAt = user.emailVerifiedAt.orEmpty())

        map[ACCESS_TOKEN_KEY] = AccessToken(from.data.token)

        return map
    }

    fun mapFromDB(from: DBUserSession): UserSession {
        return UserSession(fullName = from.fullName.orEmpty(),
                firstName = from.firstName.orEmpty(),
                lastName = from.lastName.orEmpty(),
                email = from.email.orEmpty(),
                photoUrl = from.photoUrl.orEmpty(),
                uid = from.uid,
                dateOfBirth = from.dateOfBirth.orEmpty(),
                emailVerifiedAt = from.emailVerifiedAt.orEmpty())
    }

    fun mapFromDB(from: DBToken): AccessToken {
        return AccessToken(token = from.token, refresh = from.refresh)
    }

    companion object {
        const val USER_KEY = "user"
        const val ACCESS_TOKEN_KEY = "accessToken"

        private var INSTANCE: UserSessionMapper? = null

        fun getInstance(): UserSessionMapper =
                INSTANCE ?: synchronized(this) {
                    INSTANCE
                            ?: UserSessionMapper().also { INSTANCE = it }
                }
    }
}

fun Map<String, Any>.getUserSession(): UserSession {
    return this[UserSessionMapper.USER_KEY] as UserSession
}

fun Map<String, Any>.getAccessToken(): AccessToken {
    return this[UserSessionMapper.ACCESS_TOKEN_KEY] as AccessToken
}
