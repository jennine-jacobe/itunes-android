package com.appetiser.module.data.poko

import com.appetiser.module.network.BASE_URL

class CountryCode(
    val id: Long = 0,
    val name: String = "",
    val callingCode: String = "",
    val flag: String = ""
) {

    val flagUrl = "$BASE_URL$flag"
}
