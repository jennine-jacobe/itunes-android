package com.appetiser.module.data

import com.appetiser.module.data.mapper.MoviesMapper
import com.appetiser.module.data.mapper.UserSessionMapper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MapperModule {

    @Provides
    @Singleton
    fun providesUserSessionMapper(): UserSessionMapper {
        return UserSessionMapper.getInstance()
    }

    @Provides
    @Singleton
    fun providesMoviesMapper(): MoviesMapper {
        return MoviesMapper.getInstance()
    }
}
