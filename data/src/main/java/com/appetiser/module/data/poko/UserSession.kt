package com.appetiser.module.data.poko

data class UserSession(
    val lastName: String = "",
    val firstName: String = "",
    val fullName: String = "",
    val email: String = "",
    val photoUrl: String = "",
    val emailVerifiedAt: String? = "",
    val dateOfBirth: String? = "",
    val uid: String = ""
) {
    override fun toString(): String {
        return "$fullName | email = $email"
    }
}
