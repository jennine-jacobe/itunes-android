package com.appetiser.module.data.poko

import android.os.Parcelable
import com.appetiser.module.data.ext.getYear
import com.appetiser.module.data.ext.millisToTime
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Movie(
    val trackId: Int = 0,
    val trackName: String = "",
    val primaryGenreName: String = "",
    val releaseDate: String = "",
    val trackTimeMillis: Long = 0,
    val currency: String = "",
    val trackPrice: Double = 0.0,
    val longDescription: String = "",
    val artworkUrl100: String = ""
) : Parcelable {

    fun formattedTrackSummary(): String {
        return "$primaryGenreName | ${releaseDate.getYear()} | ${trackTimeMillis.millisToTime()}"
    }

    fun formattedPrice(): String {
        return "$currency $trackPrice"
    }
}
