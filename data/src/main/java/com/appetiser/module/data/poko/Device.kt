package com.appetiser.module.data.poko

data class Device(var width: Int, var height: Int)
