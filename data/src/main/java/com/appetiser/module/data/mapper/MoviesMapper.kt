package com.appetiser.module.data.mapper

import com.appetiser.module.data.poko.Movie
import com.appetiser.module.local.model.DBMovies
import com.appetiser.module.network.response.movies.MoviesSearchResponse

@Suppress("UNCHECKED_CAST")
class MoviesMapper private constructor() {

    fun mapFromDomain(from: MoviesSearchResponse): Map<String, Any> {
        val itunes = from.results
        val map = mutableMapOf<String, Any>()

        map[MOVIES_KEY] = itunes.map {
            Movie(
                trackId = it.trackId,
                trackName = it.trackName,
                primaryGenreName = it.primaryGenreName,
                releaseDate = it.releaseDate,
                trackTimeMillis = it.trackTimeMillis,
                currency = it.currency,
                trackPrice = it.trackPrice,
                longDescription = it.longDescription,
                artworkUrl100 = it.artworkUrl100
            )
        }

        return map
    }

    fun mapFromDB(from: List<DBMovies>): Map<String, Any> {
        val map = mutableMapOf<String, Any>()
        map[MOVIES_KEY] = from.map {
            Movie(
                trackId = it.trackId,
                trackName = it.trackName,
                primaryGenreName = it.primaryGenreName,
                releaseDate = it.releaseDate,
                trackTimeMillis = it.trackTimeMillis,
                currency = it.currency,
                trackPrice = it.trackPrice,
                longDescription = it.longDescription,
                artworkUrl100 = it.artworkUrl100
            )
        }
        return map
    }

    companion object {
        const val MOVIES_KEY = "movie"
        private var INSTANCE: MoviesMapper? = null

        fun getInstance(): MoviesMapper =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: MoviesMapper().also { INSTANCE = it }
            }
    }
}

fun Map<String, Any>.getMovies(): List<Movie> {
    return this[MoviesMapper.MOVIES_KEY] as? List<Movie> ?: return listOf()
}
