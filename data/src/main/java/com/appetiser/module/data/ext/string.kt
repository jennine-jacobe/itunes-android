package com.appetiser.module.data.ext

import java.text.SimpleDateFormat
import java.util.*

/**
 * Gets year of date from Movie - First 4 characters
 */
fun String.getYear(): String {
    return this.substring(0, 4)
}

/**
 * Converted track time millis to date and formatted to a pattern
 */
fun Long.millisToTime(): String {
    return SimpleDateFormat("hh'h'mm'm'", Locale.getDefault()).format(Date(this))
}
