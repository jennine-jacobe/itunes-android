[![ktlint](https://img.shields.io/badge/code%20style-%E2%9D%A4-FF4081.svg)](https://ktlint.github.io/)

Language: Kotlin    
Architecture: MVVM    
Default Tools :  Dagger2, RxJava2, Architecture Components and etc.  see. app's build.gradle    
List of 3rd Party Libraries that are commonly used are found on "Config.kt"    

> Note : You can use any architecture layers as long as the MVVM and Dagger2 remains

  
**Code Style & Formatting**  
  
- Add Android Kotlin Style Guide [https://developer.android.com/kotlin/style-guide](https://developer.android.com/kotlin/style-guide)  
- Add KtLint for Kotlin linter and formatting [https://github.com/pinterest/ktlint](https://github.com/pinterest/ktlint)  

**Setup ktlint to your project**
- Copy the file ktlint.gradle and place it at the root of your project
- In your `app/build.gradle` file, add these lines in your android block `check.dependsOn ktlint` and `preBuild.dependsOn ktlint`
- To verify if the tasks are added, run `./gradlew tasks` and you should find the tasks `ktlint` and `ktlintFormat`
- Use `ktlint` to check the formatting
- Use `ktlintFormat` to fix the formatting
- When `ktintFormat` still fails, then you have a serious problem on your code not adhering to the guidelines
- Lastly, add a ktlint badge on your project because... why not? :)

```
android {
    compileSdkVersion 28
    ...

    check.dependsOn ktlint

    preBuild.dependsOn ktlint
}
```

  
## Project Structure
  
- [MVVM and](https://github.com/googlesamples/android-architecture) Repository pattern (optional), you can use any added layers as long as your base architecture is MVVM  
- [Dagger 2 Injector](https://medium.com/@iammert/new-android-injector-with-dagger-2-part-1-8baa60152abe)  
- [Architecture Components](https://developer.android.com/topic/libraries/architecture/)  [(LifeCycle,](https://developer.android.com/topic/libraries/architecture/) [Room, LiveData, ViewModel)](https://developer.android.com/topic/libraries/architecture/)  [(Optional)](https://developer.android.com/topic/libraries/architecture/)  
  
**Project Folder Structure**  **(for** **discussion)**  
  
We are using package by feature [https://medium.com/mindorks/pbf-package-by-feature-no-more-pbl-package-by-layer-50b8a9d54ae8](https://medium.com/mindorks/pbf-package-by-feature-no-more-pbl-package-by-layer-50b8a9d54ae8)  , if you have something in mind, please try to reach us by creating an issue ticket and we will talk about it [[https://gitlab.com/appetiser/android-baseplate/issues](https://gitlab.com/appetiser/android-baseplate/issues)]
    
#### Class and package naming

_Class name_ **(PascalCase)**  
  
_Example:_  
- LoginActivity  
- LogoutActivity  
- RegisterFragment  
  
_Package name_ **(lower case concatenated words)**  
  
  Example:
- myfirstapp  
- _myprofilepage_  
  
  
#### Project Configuration
**Build Variants**
  
- internalDebug
- productionRelease
  
### FAQ

**Can we remove ktlint?**
- No, ktlint helps us a lot, ktlint will do the code formatting for you. 
- Ktlint will only get triggered if you try to commit your code using `git commit`. It will auto format your code based on ktlint standards. If you will get an error, you need to fix it or else you can't `push` your code. :)
- There's a scenario that someone will contribute to your project and you don't have the same coding style, if they push their code to your project, there is a possibility that they can push unnecessary line of code (too much break line, white spaces, etc)

**Can we change the MVVM architecture?**
- No, because most of our apps are using MVVM and if you encountered some issues about your project, we can easily help you.

**Can we add new layer/s in our MVVM architecture?**
- Yes, as long as the core architecture is MVVM.

**Can we remove dagger?**
- For now, No, Dagger helps us a lot. If you're not familiar with dagger, visit this url `https://appetiser.com.au/blog/dagger-a-new-dependency-injection-for-java-and-android/`

**Where can I add my gradle dependencies?**
- buildSrc/src/main/java/Config.kt

**Can I change the build variants?**
- Feel free


## List of modules and dependency
#### android-baseplate-domain module ####
This module communicates to our [baseplate backend](http://128.199.193.26/api/documentation). For now, most of our sprint 1 projects are based [baseplate backend](http://128.199.193.26/api/documentation).
If you found some bugs or you want to contribute. Please visit [URL](https://gitlab.com/appetiser/android-baseplate-domain?nav_source=navbar) . 

#### android-baseplate-persistence module
This module is used for caching auth user and token, We are using `ROOM`.

#### auth_modern module
This module is for modern flow layout resources

#### auth_traditional module
This module is for traditional flow layout resources