package com.appetiser.itunes.features.main

import com.appetiser.module.data.poko.UserSession

sealed class MainState {

    data class FetchUserInfo(val user: UserSession) : MainState()

    object LogoutSuccess : MainState()

    object ShowProgressLoading : MainState()

    object HideProgressLoading : MainState()
}
