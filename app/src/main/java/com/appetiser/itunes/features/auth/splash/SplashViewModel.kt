package com.appetiser.itunes.features.auth.splash

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.itunes.base.BaseViewModel
import com.appetiser.itunes.data.source.repository.AuthRepository
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SplashViewModel @Inject constructor(private val repository: AuthRepository) : BaseViewModel() {

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
    }

    private val _state: MutableLiveData<SplashState> by lazy {
        MutableLiveData<SplashState>()
    }

    val state: LiveData<SplashState>
        get() {
            return _state
        }

    fun isUserLoggedIn() {
        _state.value = SplashState.ShowProgressLoading
        disposables.add(
            repository.isUserLoggedIn()
                .delay(500, TimeUnit.MILLISECONDS)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {
                    _state.value = SplashState.HideProgressLoading
                }
                .subscribeBy(
                    onSuccess = {
                        if (it) {
                            _state.value = SplashState.UserSessionLoggedIn
                        } else {
                            _state.value = SplashState.UserSessionNotLoggedIn
                        }
                    },
                    onError = {
                    }
                )
        )
    }
}
