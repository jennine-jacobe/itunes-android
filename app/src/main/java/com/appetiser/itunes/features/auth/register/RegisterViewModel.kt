package com.appetiser.itunes.features.auth.register

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.itunes.base.BaseViewModel
import com.appetiser.itunes.data.source.repository.AuthRepository
import com.appetiser.module.data.mapper.getUserSession
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class RegisterViewModel @Inject constructor(
    private val repository: AuthRepository
) : BaseViewModel() {

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
    }

    private val _state: MutableLiveData<RegisterState> by lazy {
        MutableLiveData<RegisterState>()
    }

    val state: LiveData<RegisterState>
        get() {
            return _state
        }

    fun signUp(fullName: String, email: String, password: String, retypePassword: String): Disposable {
        var lastName = ""
        var firstName = ""

        try {
            if (fullName.split("\\w+").isNotEmpty()) {
                lastName = fullName.substring(fullName.lastIndexOf(" ") + 1)
                firstName = fullName.substring(0, fullName.lastIndexOf(" "))
            } else {
                firstName = fullName
            }
        } catch (e: StringIndexOutOfBoundsException) {
            lastName = ""
            firstName = fullName
        }

        _state.value = RegisterState.ShowProgressLoading

        return repository.signup(email, password, retypePassword, firstName, lastName, "", "", "", "")
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doFinally {
                _state.value = RegisterState.HideProgressLoading
            }
            .subscribeBy(onSuccess = {
                val user = it.getUserSession()

                if (user.uid.isNotEmpty()) {
                    _state.value = RegisterState.RegisterSuccessFull
                }
            }, onError = {
                _state.value = RegisterState.Error(it)
            })
    }
}
