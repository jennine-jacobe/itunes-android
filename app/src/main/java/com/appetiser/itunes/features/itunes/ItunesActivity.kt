package com.appetiser.itunes.features.itunes

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.appetiser.itunes.R
import com.appetiser.itunes.base.BaseViewModelActivity
import com.appetiser.itunes.databinding.ActivityItunesBinding
import com.appetiser.itunes.features.itunes.movies.MoviesViewModel
import kotlinx.android.synthetic.main.activity_itunes.*

class ItunesActivity : BaseViewModelActivity<ActivityItunesBinding, MoviesViewModel>(),
    NavController.OnDestinationChangedListener {

    companion object {
        fun openActivity(context: Context) {
            val intent = Intent(context, ItunesActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(intent)
        }
    }

    private val navController: NavController by lazy {
        findNavController(R.id.nav_host_fragment)
    }

    override fun getLayoutId(): Int = R.layout.activity_itunes

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(itunes_toolbar)
        /* Binds the actionbar with the navigation controller */
        setupActionBarWithNavController(navController)
        /* Adds callback on navigation controller for when the destination or its arguments change. */
        navController.addOnDestinationChangedListener(this)
    }

    /**
     * Adds action to actionbar back navigation
     */
    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    /**
     * Disables collapsing toolbar on details page and adds animation when
     * returning to list page.
     */
    override fun onDestinationChanged(controller: NavController, destination: NavDestination, arguments: Bundle?) {
        if (destination.id == R.id.moviesFragment) {
            collapsing_appbar_layout.setExpanded(true, true)
            collapsing_toolbar_layout.title = getString(R.string.itunes_title)
        } else {
            collapsing_appbar_layout.setExpanded(false, false)
            collapsing_toolbar_layout.title = " "
        }
    }
}
