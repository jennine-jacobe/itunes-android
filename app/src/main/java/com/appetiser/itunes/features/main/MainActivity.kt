package com.appetiser.itunes.features.main

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.appetiser.itunes.R
import com.appetiser.itunes.base.BaseViewModelActivity
import com.appetiser.itunes.databinding.ActivityMainBinding
import com.appetiser.itunes.features.auth.splash.SplashScreenActivity
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast

class MainActivity : BaseViewModelActivity<ActivityMainBinding, MainViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(intent)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpViewModels()

        disposables.add(binding.logout.ninjaTap {
            viewModel.logoutUserSession()
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.fetchUserSession()
    }

    @SuppressLint("SetTextI18n")
    private fun setUpViewModels() {
        viewModel.state.observe(this, Observer {
            when (it) {
                is MainState.FetchUserInfo -> {
                    binding.userInfo.text = "Welcome to Main Screen ${it.user}"
                }

                is MainState.LogoutSuccess -> {
                    SplashScreenActivity.openActivity(this)
                    finish()
                }

                is MainState.ShowProgressLoading -> {
                    toast("Sending Request")
                }
            }
        })
    }
}
