package com.appetiser.itunes.features.auth.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.appetiser.auth_traditional.databinding.ActivityTraditionalLoginBinding
import com.appetiser.itunes.R
import com.appetiser.itunes.base.BaseViewModelActivity
import com.appetiser.itunes.features.auth.forgotpassword.ForgotPasswordActivity
import com.appetiser.itunes.features.itunes.ItunesActivity
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.appetiser.module.network.response.error.ResponseError
import sampleData.SampleData

class LoginActivity : BaseViewModelActivity<ActivityTraditionalLoginBinding, LoginViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            val intent = Intent(context, LoginActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_traditional_login

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViews()
        setupViewModels()
        setupToolbar()
    }

    private fun setupViews() {

        binding.etEmail.setText(SampleData.USER_NAME)
        binding.etPassword.setText(SampleData.CORRECT_PASSWORD)
    }

    private fun setupToolbar() {
        setToolbarTitle(getString(R.string.login))
        enableToolbarHomeIndicator()
    }

    private fun setupViewModels() {
        viewModel.state.observe(this, Observer { it ->
            when (it) {
                is LoginState.LoginSuccess -> {
                    ItunesActivity.openActivity(this)
                }

                is LoginState.Error -> {
                    // show error message
                    ResponseError.getError(it.throwable,
                        ResponseError.ErrorCallback(httpExceptionCallback = {
                            toast("Error $it")
                        }))
                }
                is LoginState.ShowProgressLoading -> {
                    toast("Sending request")
                }
            }
        })

        disposables.add(binding.btnLogin.ninjaTap {
            val email = binding.etEmail.text.toString()
            val password = binding.etPassword.text.toString()

            when {
                email.isEmpty() -> toast(getString(R.string.email_must_not_be_empty))
                password.isEmpty() -> toast(getString(R.string.password_must_not_be_empty))
                else -> viewModel.login(email, password)
            }
        })

        disposables.add(binding.forgotPassword.ninjaTap {
            ForgotPasswordActivity.openActivity(this)
        })
    }
}
