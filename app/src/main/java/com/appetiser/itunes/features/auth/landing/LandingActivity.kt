package com.appetiser.itunes.features.auth.landing

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.auth_traditional.databinding.ActivityTraditionalLandingBinding
import com.appetiser.itunes.R
import com.appetiser.itunes.base.BaseActivity
import com.appetiser.itunes.features.auth.login.LoginActivity
import com.appetiser.itunes.features.auth.register.RegisterActivity
import com.appetiser.itunes.features.itunes.ItunesActivity
import com.appetiser.module.common.ninjaTap

class LandingActivity : BaseActivity<ActivityTraditionalLandingBinding>() {

    companion object {
        fun openActivity(context: Context) {
            context.startActivity(Intent(context, LandingActivity::class.java))
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_traditional_landing

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViews()
    }

    private fun setupViews() {
        disposables.add(binding.loginHere.ninjaTap {
            LoginActivity.openActivity(this)
        })

        disposables.add(binding.btnJoin.ninjaTap {
            RegisterActivity.openActivity(this)
        })

        disposables.add(binding.continueAsGuest.ninjaTap {
            ItunesActivity.openActivity(this)
        })
    }
}
