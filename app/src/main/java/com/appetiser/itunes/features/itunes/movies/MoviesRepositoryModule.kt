package com.appetiser.itunes.features.itunes.movies

import com.appetiser.module.network.ApiServices
import com.appetiser.itunes.data.source.local.MoviesLocalSource
import com.appetiser.itunes.data.source.remote.MoviesRemoteSource
import com.appetiser.itunes.data.source.repository.MoviesRepository
import com.appetiser.itunes.di.scopes.ActivityScope
import com.appetiser.module.data.mapper.MoviesMapper
import com.appetiser.module.local.AppDatabase
import dagger.Module
import dagger.Provides

@Module
class MoviesRepositoryModule {

    @ActivityScope
    @Provides
    fun providesMoviesLocalSource(database: AppDatabase, mapper: MoviesMapper):
        MoviesLocalSource = MoviesLocalSource(database, mapper)

    @ActivityScope
    @Provides
    fun providesMoviesRemoteSource(apiServices: ApiServices, mapper: MoviesMapper):
        MoviesRemoteSource = MoviesRemoteSource(apiServices, mapper)

    @ActivityScope
    @Provides
    fun providesMoviesRepository(remote: MoviesRemoteSource, local: MoviesLocalSource): MoviesRepository =
        MoviesRepository(remote, local)
}
