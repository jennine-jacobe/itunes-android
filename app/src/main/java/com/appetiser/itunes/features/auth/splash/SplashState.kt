package com.appetiser.itunes.features.auth.splash

sealed class SplashState {

    object UserSessionLoggedIn : SplashState()

    object UserSessionNotLoggedIn : SplashState()

    object ShowProgressLoading : SplashState()

    object HideProgressLoading : SplashState()
}
