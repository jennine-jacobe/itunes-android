package com.appetiser.itunes.features.itunes.details

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.View
import androidx.navigation.fragment.navArgs
import com.appetiser.itunes.R
import com.appetiser.itunes.base.BaseFragment
import com.appetiser.module.data.poko.Movie
import com.appetiser.itunes.databinding.FragmentDetailBinding

/**
 * [DetailFragment] shows detailed view of selected item from list
 */
class DetailFragment : BaseFragment<FragmentDetailBinding>() {

    private val args: DetailFragmentArgs by navArgs()
    private val movie: Movie by lazy {
        args.movie
    }

    override fun getLayoutId(): Int = R.layout.fragment_detail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /*  Sets the transition for shared elements */
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    /**
	 * Sets the values to be displayed on each views
	 */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.movie = movie
    }
}
