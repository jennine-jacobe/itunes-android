package com.appetiser.itunes.features.itunes.movies

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.appetiser.itunes.R
import com.appetiser.itunes.base.BaseFragment
import com.appetiser.itunes.databinding.FragmentMoviesBinding

class MoviesFragment : BaseFragment<FragmentMoviesBinding>() {

    lateinit var viewModel: MoviesViewModel

    override fun getLayoutId(): Int = R.layout.fragment_movies

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let {
            viewModel = ViewModelProviders.of(it).get(MoviesViewModel::class.java)
            binding.viewModel = viewModel
            viewModel.getMovies()
        }
    }
}
