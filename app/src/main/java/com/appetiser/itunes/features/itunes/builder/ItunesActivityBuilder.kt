package com.appetiser.itunes.features.itunes.builder

import com.appetiser.itunes.di.scopes.FragmentScope
import com.appetiser.itunes.features.itunes.movies.MoviesRepositoryModule
import com.appetiser.itunes.features.itunes.details.DetailFragment
import com.appetiser.itunes.features.itunes.movies.MoviesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ItunesActivityBuilder {

    @FragmentScope
    @ContributesAndroidInjector(modules = [MoviesRepositoryModule::class])
    abstract fun contributeMoviesFragment(): MoviesFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeDetailFragment(): DetailFragment
}
