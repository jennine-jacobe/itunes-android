package com.appetiser.itunes.features.auth.forgotpassword

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.itunes.base.BaseViewModel
import com.appetiser.itunes.data.source.repository.AuthRepository
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class ForgotPasswordViewModel @Inject constructor(
    private val repository: AuthRepository
) :
    BaseViewModel() {

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
    }

    private val _state: MutableLiveData<ForgotPasswordState> by lazy {
        MutableLiveData<ForgotPasswordState>()
    }

    val state: LiveData<ForgotPasswordState>
        get() {
            return _state
        }

    fun forgotPassword(email: String): Disposable {
        _state.value = ForgotPasswordState.ShowProgressLoading
        return repository.forgotPassword(email)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {
                    _state.value = ForgotPasswordState.HideProgressLoading
                }
                .subscribe({
                    if (it) {
                        _state.value = ForgotPasswordState.Success(true)
                    }
                }, {
                    _state.value = ForgotPasswordState.Error(it)
                })
    }
}
