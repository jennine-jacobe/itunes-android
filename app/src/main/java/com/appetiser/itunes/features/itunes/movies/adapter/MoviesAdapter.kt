package com.appetiser.itunes.features.itunes.movies.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.itunes.R
import com.appetiser.itunes.base.BaseRecycleViewAdapter
import com.appetiser.module.data.poko.Movie
import com.appetiser.itunes.databinding.ItemMovieBinding
import com.appetiser.itunes.features.itunes.movies.MoviesFragmentDirections

/**
 * MoviesAdapter is a [RecyclerView.Adapter] that can display list of [Movie]
 */
class MoviesAdapter : BaseRecycleViewAdapter() {

    /* Inflate a item_movie layout from XML and return the holder */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemMovieBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    /* Populate data into the item through holder  */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        /* Get Movie data model based on position */
        val item = getItemByPosition(position) as Movie
        holder as ViewHolder
        /* Bind data model into the view holder */
        holder.bind(item)
    }

    inner class ViewHolder(val binding: ItemMovieBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.setClickListener {
                binding.movie?.let { movies ->
                    navigateToDetails(movies, it)
                }
            }
        }

        private fun navigateToDetails(movie: Movie, view: View) {
            /* Set transition name and pass shared element for animation */
            ViewCompat.setTransitionName(view, view.context.getString(R.string.img_artwork))
            val extras = FragmentNavigatorExtras(view to view.context.getString(R.string.img_artwork))
            /* Specify fragment direction and navigate */
            val action = MoviesFragmentDirections.actionMoviesFragmentToDetailFragment(movie)
            view.findNavController().navigate(action, extras)
        }

        fun bind(movie: Movie) {
            binding.movie = movie
            binding.executePendingBindings()
        }
    }
}
