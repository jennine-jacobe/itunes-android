package com.appetiser.itunes.features.auth.register

sealed class RegisterState {

    object RegisterSuccessFull : RegisterState()

    data class Error(val throwable: Throwable) : RegisterState()

    object ShowProgressLoading : RegisterState()

    object HideProgressLoading : RegisterState()
}
