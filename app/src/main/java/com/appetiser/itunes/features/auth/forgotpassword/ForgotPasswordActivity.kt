package com.appetiser.itunes.features.auth.forgotpassword

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.appetiser.auth_traditional.databinding.ActivityTraditionalForgotPasswordBinding
import com.appetiser.itunes.R
import com.appetiser.itunes.base.BaseViewModelActivity
import com.appetiser.module.common.isEmailValid
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.appetiser.module.network.response.error.ResponseError
import sampleData.SampleData

class ForgotPasswordActivity : BaseViewModelActivity<ActivityTraditionalForgotPasswordBinding, ForgotPasswordViewModel>() {

    override fun getLayoutId(): Int = R.layout.activity_traditional_forgot_password

    companion object {
        fun openActivity(context: Context) {
            context.startActivity(Intent(context, ForgotPasswordActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        setUpViewModels()
        binding.etEmail.setText(SampleData.USER_NAME)
    }

    private fun setUpViewModels() {
        viewModel.state.observe(this, Observer {
            when (it) {
                is ForgotPasswordState.Success -> {
                    toast(getString(R.string.forgot_password_success_message, binding.etEmail.text.toString()))
                }

                is ForgotPasswordState.Error -> {
                    ResponseError.getError(it.throwable,
                        ResponseError.ErrorCallback(httpExceptionCallback = {
                            toast("Error $it")
                        }))
                }

                is ForgotPasswordState.ShowProgressLoading -> {
                    toast("Sending request")
                }
            }
        })

        disposables.add(binding.btnContinue.ninjaTap {
            val email = binding.etEmail.text.toString()
            when {
                email.isEmpty() -> toast(getString(R.string.email_must_not_be_empty))
                !email.isEmailValid() -> toast(getString(R.string.invalid_email))
                else -> viewModel.forgotPassword(email)
            }
        })
    }

    private fun setupToolbar() {
        setToolbarTitle(getString(R.string.forgot_password))
        enableToolbarHomeIndicator()
    }
}
