package com.appetiser.itunes.features.auth

import android.content.SharedPreferences
import com.appetiser.itunes.data.source.local.AuthLocalSource
import com.appetiser.itunes.data.source.remote.AuthRemoteSource
import com.appetiser.itunes.data.source.repository.AuthRepository
import com.appetiser.itunes.di.scopes.ActivityScope
import com.appetiser.module.data.mapper.UserSessionMapper
import com.appetiser.module.local.AppDatabase
import com.appetiser.module.network.BaseplateApiServices
import dagger.Module
import dagger.Provides

@Module
class AuthRepositoryModule {

    @ActivityScope
    @Provides
    fun providesAuthLocalSource(sharefPref: SharedPreferences, database: AppDatabase, mapper: UserSessionMapper):
            AuthLocalSource = AuthLocalSource(sharefPref, database, mapper)

    @ActivityScope
    @Provides
    fun providesAuthRemoteSource(baseplateApiServices: BaseplateApiServices, mapper: UserSessionMapper):
            AuthRemoteSource = AuthRemoteSource(baseplateApiServices, mapper)

    @ActivityScope
    @Provides
    fun providesAuthRepository(remote: AuthRemoteSource, local: AuthLocalSource): AuthRepository =
            AuthRepository(remote, local)
}
