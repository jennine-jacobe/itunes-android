package com.appetiser.itunes.features.auth.register

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import androidx.appcompat.widget.*
import androidx.lifecycle.Observer
import com.appetiser.auth_traditional.databinding.ActivityTraditionalRegisterBinding
import com.appetiser.itunes.R
import com.appetiser.itunes.base.BaseViewModelActivity
import com.appetiser.itunes.features.main.MainActivity
import com.appetiser.module.common.isEmailValid
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.appetiser.module.network.response.error.ResponseError
import sampleData.SampleData

class RegisterActivity : BaseViewModelActivity<ActivityTraditionalRegisterBinding, RegisterViewModel>(), AdapterView.OnItemClickListener {

    companion object {
        fun openActivity(context: Context) {
            context.startActivity(Intent(context, RegisterActivity::class.java))
        }
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
    }

    override fun getLayoutId(): Int = R.layout.activity_traditional_register

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN and
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
        )

        setupViewModels()
        setupToolbar()
        setupSampleData()
    }

    private fun setupSampleData() {
        binding.etEmail.setText(SampleData.USER_NAME)
        binding.etFullName.setText(SampleData.USER_FULL_NAME)
        binding.etPassword.setText(SampleData.CORRECT_PASSWORD)
        binding.etRetypePassword.setText(SampleData.CORRECT_PASSWORD)
    }

    private fun setupToolbar() {
        setToolbarTitle(getString(R.string.register))
        enableToolbarHomeIndicator()
    }

    private fun setupViewModels() {
        viewModel.state.observe(this, Observer {
            when (it) {
                is RegisterState.RegisterSuccessFull -> {
                    MainActivity.openActivity(this)
                }

                is RegisterState.Error -> {
                    // show error message
                    ResponseError.getError(it.throwable,
                        ResponseError.ErrorCallback(httpExceptionCallback = {
                            toast(it)
                        }))
                }

                is RegisterState.ShowProgressLoading -> {
                    toast("Sending request")
                }
            }
        })

        disposables.add(binding.btnSignup.ninjaTap {
            val email = binding.etEmail.text.toString()
            val fullName = binding.etFullName.text.toString()
            val password = binding.etPassword.text.toString()
            val retypePassword = binding.etRetypePassword.text.toString()

            when {
                email.isEmpty() -> toast(getString(R.string.email_must_not_be_empty))
                !email.isEmailValid() -> toast(getString(R.string.invalid_email))
                fullName.isEmpty() -> toast(getString(R.string.full_name_must_not_be_empty))
                password.isEmpty() -> toast(getString(R.string.password_must_not_be_empty))
                retypePassword.isEmpty() -> toast(getString(R.string.confirm_password_must_not_be_empty))
                password.length < 6 -> toast(getString(R.string.password_must_be_more_than_six))
                retypePassword.length < 6 -> toast(getString(R.string.confirm_password_must_be_more_than_six))
                password != retypePassword -> toast(getString(R.string.password_doesnt_match))
                else -> viewModel.signUp(fullName, email, password, retypePassword)
            }
        })
    }
}
