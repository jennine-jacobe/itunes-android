package com.appetiser.itunes.features.main

import com.appetiser.itunes.features.auth.AuthRepositoryModule
import dagger.Module

@Module(includes = [(AuthRepositoryModule::class)])
class MainRepositoryModule
