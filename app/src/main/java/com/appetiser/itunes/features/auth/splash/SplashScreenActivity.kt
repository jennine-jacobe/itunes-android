package com.appetiser.itunes.features.auth.splash

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.appetiser.itunes.R
import com.appetiser.itunes.base.BaseViewModelActivity
import com.appetiser.itunes.features.auth.landing.LandingActivity
import com.appetiser.itunes.features.itunes.ItunesActivity

class SplashScreenActivity : BaseViewModelActivity<ViewDataBinding, SplashViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            context.startActivity(Intent(context, SplashScreenActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViewModels()
    }

    override fun getLayoutId(): Int = R.layout.activity_splash

    private fun setupViewModels() {
        viewModel.state.observe(this, Observer { state ->
            when (state) {
                is SplashState.UserSessionLoggedIn -> {
                    ItunesActivity.openActivity(this)
                    finish()
                }

                is SplashState.UserSessionNotLoggedIn -> {
                    LandingActivity.openActivity(this)
                    finish()
                }
            }
        })

        viewModel.isUserLoggedIn()
    }
}
