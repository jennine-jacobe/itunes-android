package com.appetiser.itunes.features.itunes.movies

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.itunes.base.BaseViewModel
import com.appetiser.itunes.data.source.repository.MoviesRepository
import com.appetiser.itunes.features.itunes.movies.adapter.MoviesAdapter
import com.appetiser.module.data.mapper.getMovies
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class MoviesViewModel @Inject constructor(
    private val repository: MoviesRepository
) : BaseViewModel() {

    val moviesAdapter: MoviesAdapter = MoviesAdapter()

    override fun isFirstTimeUiCreate(bundle: Bundle?) {}

    private val _isLoading: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }

    val isLoading: LiveData<Boolean>
        get() {
            return _isLoading
        }

    fun getMovies() {
        _isLoading.value = true
        disposables.add(
            repository.getMovies()
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {
                    _isLoading.value = false
                }
                .subscribeBy(
                    onSuccess = {
                        val list = it.getMovies()
                        moviesAdapter.removeAllData()
                        moviesAdapter.addAllData(list)
                        moviesAdapter.notifyDataSetChanged()
                    },
                    onError = {
                        // TODO: Handle error
                    }
                )
        )
    }
}
