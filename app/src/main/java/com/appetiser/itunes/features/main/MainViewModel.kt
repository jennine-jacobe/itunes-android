package com.appetiser.itunes.features.main

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.itunes.base.BaseViewModel
import com.appetiser.itunes.data.source.repository.AuthRepository
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val repository: AuthRepository
) : BaseViewModel() {

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
    }

    private val _state: MutableLiveData<MainState> by lazy {
        MutableLiveData<MainState>()
    }

    val state: LiveData<MainState>
        get() {
            return _state
        }

    fun fetchUserSession() {
        _state.value = MainState.ShowProgressLoading
        disposables.add(
            repository.getUserSession()
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {
                    _state.value = MainState.HideProgressLoading
                }
                .subscribeBy(
                    onSuccess = {
                        _state.value = MainState.FetchUserInfo(it)
                    },
                    onError = {
                    }
                )
        )
    }

    fun logoutUserSession() {
        _state.value = MainState.ShowProgressLoading
        disposables.add(repository.logout()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(onComplete = {
                _state.value = MainState.LogoutSuccess
            }, onError = {
            }))
    }
}
