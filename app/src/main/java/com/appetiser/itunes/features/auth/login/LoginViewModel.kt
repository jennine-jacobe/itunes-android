package com.appetiser.itunes.features.auth.login

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.itunes.base.BaseViewModel
import com.appetiser.itunes.data.source.repository.AuthRepository
import com.appetiser.module.data.mapper.getUserSession
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val repository: AuthRepository
) :
    BaseViewModel() {

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
    }

    private val _state: MutableLiveData<LoginState> by lazy {
        MutableLiveData<LoginState>()
    }

    val state: LiveData<LoginState>
        get() {
            return _state
        }

    fun login(email: String, password: String) {
        _state.value = LoginState.ShowProgressLoading
        disposables.add(
            repository.login(email, password)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doFinally {
                    _state.value = LoginState.HideProgressLoading
                }
                .subscribeBy(
                    onSuccess = {
                        val user = it.getUserSession()

                        if (user.uid.isNotEmpty()) {
                            _state.value = LoginState.LoginSuccess(user)
                        }
                    },
                    onError = {
                        _state.value = LoginState.Error(it)
                    }
                )
        )
    }
}
