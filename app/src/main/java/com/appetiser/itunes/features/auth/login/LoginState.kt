package com.appetiser.itunes.features.auth.login

import com.appetiser.module.data.poko.UserSession

sealed class LoginState {

    data class LoginSuccess(val user: UserSession) : LoginState()

    data class Error(val throwable: Throwable) : LoginState()

    object ShowProgressLoading : LoginState()

    object HideProgressLoading : LoginState()
}
