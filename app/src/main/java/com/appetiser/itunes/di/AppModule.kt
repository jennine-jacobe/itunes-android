package com.appetiser.itunes.di

import android.content.Context
import com.appetiser.itunes.ItunesApplication
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class AppModule {

    @Singleton
    @Binds
    abstract fun providesApplicationContext(app: ItunesApplication): Context
}
