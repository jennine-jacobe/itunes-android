package com.appetiser.itunes.di.builders

import com.appetiser.itunes.di.scopes.ActivityScope
import com.appetiser.itunes.features.auth.AuthRepositoryModule
import com.appetiser.itunes.features.auth.forgotpassword.ForgotPasswordActivity
import com.appetiser.itunes.features.auth.landing.LandingActivity
import com.appetiser.itunes.features.auth.login.LoginActivity
import com.appetiser.itunes.features.auth.register.RegisterActivity
import com.appetiser.itunes.features.auth.splash.SplashScreenActivity
import com.appetiser.itunes.features.itunes.ItunesActivity
import com.appetiser.itunes.features.itunes.builder.ItunesActivityBuilder
import com.appetiser.itunes.features.main.MainActivity
import com.appetiser.itunes.features.main.MainRepositoryModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [(MainRepositoryModule::class)])
    abstract fun contributeMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeSignUpActivity(): RegisterActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeForgotPasswordActivity(): ForgotPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeSplashScreenActivity(): SplashScreenActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeLandingActivity(): LandingActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(ItunesActivityBuilder::class)])
    abstract fun contributeItunesActivity(): ItunesActivity
}
