package com.appetiser.itunes.di

import android.app.Application
import com.appetiser.itunes.ItunesApplication
import com.appetiser.itunes.di.builders.ActivityBuilder
import com.appetiser.module.StorageModule
import com.appetiser.module.data.MapperModule
import com.appetiser.module.network.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        MapperModule::class,
        StorageModule::class,
        NetworkModule::class,
        ActivityBuilder::class,
        SchedulerModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: ItunesApplication)
}
