package com.appetiser.itunes.di

import com.appetiser.itunes.utils.schedulers.BaseSchedulerProvider
import com.appetiser.itunes.utils.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SchedulerModule {

    @Provides
    @Singleton
    fun providesSchedulerSource(): BaseSchedulerProvider =
            SchedulerProvider.getInstance()
}
