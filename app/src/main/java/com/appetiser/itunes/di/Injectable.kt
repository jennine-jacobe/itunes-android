package com.appetiser.itunes.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
