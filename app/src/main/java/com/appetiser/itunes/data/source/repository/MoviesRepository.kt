package com.appetiser.itunes.data.source.repository

import com.appetiser.itunes.data.MoviesSource
import com.appetiser.itunes.data.source.local.MoviesLocalSource
import com.appetiser.itunes.data.source.remote.MoviesRemoteSource
import com.appetiser.module.data.mapper.getMovies
import com.appetiser.module.data.poko.Movie
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

class MoviesRepository @Inject
constructor(private val remote: MoviesRemoteSource, private val local: MoviesLocalSource) : MoviesSource {

    override fun getMovies(): Single<Map<String, Any>> {
        return remote.getMovies()
            .flatMap {
                updateLocalMovies(it).singleOrError()
            }
            .onErrorResumeNext {
                local.getMovies()
            }
    }

    override fun saveMovies(itunes: List<Movie>): Single<List<Movie>> {
        return local.saveMovies(itunes)
    }

    private fun updateLocalMovies(itunesMapper: Map<String, Any>): Observable<Map<String, Any>> {
        val itunes = itunesMapper.getMovies()
        return Observable.zip(Observable.just(itunesMapper), saveMovies(itunes)
            .toObservable(),
            BiFunction { mapper, _ ->
                return@BiFunction mapper
            })
    }
}
