package com.appetiser.itunes.data.source.repository

import com.appetiser.itunes.data.AuthSource
import com.appetiser.itunes.data.source.local.AuthLocalSource
import com.appetiser.itunes.data.source.remote.AuthRemoteSource
import com.appetiser.module.data.mapper.getAccessToken
import com.appetiser.module.data.mapper.getUserSession
import com.appetiser.module.data.poko.AccessToken
import com.appetiser.module.data.poko.UserSession
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.Function3
import javax.inject.Inject

class AuthRepository @Inject
constructor(private val remote: AuthRemoteSource, private val local: AuthLocalSource) : AuthSource {

    override fun saveCredentials(user: UserSession): Single<UserSession> {
        return local.saveCredentials(user)
    }

    override fun saveToken(token: String): Single<AccessToken> {
        return local.saveToken(token)
    }

    override fun login(email: String, password: String): Single<Map<String, Any>> {
        return remote.login(email, password)
                .flatMap {
                    saveUserCredentialsAndToken(it).singleOrError()
                }
    }

    override fun signup(email: String, password: String, passwordConfirmation: String, firstName: String, lastName: String, dob: String, country: String, mobileNumber: String, address: String): Single<Map<String, Any>> {
        return remote.signup(email, password, passwordConfirmation, firstName, lastName, dob, country, mobileNumber, address)
                .flatMap {
                    saveUserCredentialsAndToken(it).singleOrError()
                }
    }

    override fun getUserSession(): Single<UserSession> {
        return local.getUserSession()
    }

    override fun getUserToken(): Single<AccessToken> {
        return local.getUserToken()
    }

    override fun forgotPassword(email: String): Single<Boolean> {
        return remote.forgotPassword(email)
    }

    private fun saveUserCredentialsAndToken(userSessionMapper: Map<String, Any>): Observable<Map<String, Any>> {
        val userSession = userSessionMapper.getUserSession()
        val accesstoken = userSessionMapper.getAccessToken()

        return Observable.zip(Observable.just(userSessionMapper), saveCredentials(userSession)
                .toObservable(), saveToken(accesstoken.token.orEmpty())
                .toObservable(),
                Function3 { mapper, _, _ ->
                    return@Function3 mapper
                })
    }

    override fun isUserLoggedIn(): Single<Boolean> {
        return local.isUserLoggedIn()
    }

    override fun logout(): Completable {
        return local.logout()
    }
}
