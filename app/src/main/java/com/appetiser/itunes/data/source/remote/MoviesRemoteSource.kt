package com.appetiser.itunes.data.source.remote

import com.appetiser.module.network.ApiServices
import com.appetiser.itunes.data.MoviesSource
import com.appetiser.itunes.utils.ITUNES_BASE_URL
import com.appetiser.module.data.mapper.MoviesMapper
import com.appetiser.module.data.poko.Movie
import io.reactivex.Single
import javax.inject.Inject

class MoviesRemoteSource @Inject
constructor(private val apiServices: ApiServices, private val mapper: MoviesMapper) : MoviesSource {

    override fun getMovies(): Single<Map<String, Any>> {
        return apiServices.getMovies(ITUNES_BASE_URL)
            .map {
                mapper.mapFromDomain(it)
            }
    }

    override fun saveMovies(itunes: List<Movie>): Single<List<Movie>> {
        TODO("not implemented") // To change body of created functions use File | Settings | File Templates.
    }
}
