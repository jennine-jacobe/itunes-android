package com.appetiser.itunes.data.source.remote

import com.appetiser.itunes.data.AuthSource
import com.appetiser.module.data.mapper.UserSessionMapper
import com.appetiser.module.data.poko.AccessToken
import com.appetiser.module.data.poko.UserSession
import com.appetiser.module.network.BaseplateApiServices
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class AuthRemoteSource @Inject
constructor(private val baseplateApiServices: BaseplateApiServices, private val mapper: UserSessionMapper) : AuthSource {

    override fun getUserSession(): Single<UserSession> = Single.just(UserSession())

    override fun login(email: String, password: String): Single<Map<String, Any>> {
        val userMap = hashMapOf<String, String>()
        userMap["email"] = email
        userMap["password"] = password
        return baseplateApiServices.loginUser(userMap)
                .map {
                    mapper.mapFromDomain(it)
                }
    }

    override fun saveCredentials(user: UserSession): Single<UserSession> {
        TODO("not implemented") // To change body of created functions use File | Settings | File Templates.
    }

    override fun saveToken(token: String): Single<AccessToken> {
        TODO("not implemented") // To change body of created functions use File | Settings | File Templates.
    }

    override fun signup(email: String, password: String, passwordConfirmation: String, firstName: String, lastName: String, dob: String, country: String, mobileNumber: String, address: String): Single<Map<String, Any>> {
        val map = hashMapOf<String, String>()
        map["email"] = email

        if (password.isNotEmpty()) {
            map["password"] = password
        }

        if (passwordConfirmation.isNotEmpty()) {
            map["password_confirmation"] = passwordConfirmation
        }

        if (firstName.isNotEmpty()) {
            map["first_name"] = firstName
        }

        if (lastName.isNotEmpty()) {
            map["last_name"] = lastName
        }

        if (mobileNumber.isNotEmpty()) {
            map["mobile_number"] = mobileNumber
        }

        if (country.isNotEmpty()) {
            map["country"] = country
        }

        if (dob.isNotEmpty()) {
            map["dob"] = dob
        }

        if (address.isNotEmpty()) {
            map["address"] = address
        }

        return baseplateApiServices.registerUser(map)
                .map {
                    mapper.mapFromDomain(it)
                }
    }

    override fun forgotPassword(email: String): Single<Boolean> {
        val userMap = hashMapOf<String, String>()
        userMap["email"] = email
        return baseplateApiServices.sendResetPassword(userMap)
                .map { it.isResponseSuccess }
    }

    override fun isUserLoggedIn(): Single<Boolean> = Single.just(false)

    override fun getUserToken(): Single<AccessToken> = Single.just(AccessToken())

    override fun logout(): Completable = Completable.error(Throwable("Error logout"))
}
