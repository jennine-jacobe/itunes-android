package com.appetiser.itunes.data.source.local

import com.appetiser.itunes.data.MoviesSource
import com.appetiser.module.data.mapper.MoviesMapper
import com.appetiser.module.data.poko.Movie
import com.appetiser.module.local.AppDatabase
import com.appetiser.module.local.model.DBMovies
import io.reactivex.Single
import javax.inject.Inject

class MoviesLocalSource @Inject
constructor(private val database: AppDatabase, private val mapper: MoviesMapper) : MoviesSource {

    override fun saveMovies(itunes: List<Movie>): Single<List<Movie>> {
        return Single.create { emitter ->
            val dbItunes = itunes.map {
                DBMovies(
                    trackId = it.trackId,
                    trackName = it.trackName,
                    primaryGenreName = it.primaryGenreName,
                    releaseDate = it.releaseDate,
                    trackTimeMillis = it.trackTimeMillis,
                    currency = it.currency,
                    trackPrice = it.trackPrice,
                    longDescription = it.longDescription,
                    artworkUrl100 = it.artworkUrl100
                )
            }
            database.moviesDao().insertAll(dbItunes)
            emitter.onSuccess(itunes)
        }
    }

    override fun getMovies(): Single<Map<String, Any>> {
        return database.moviesDao().getAllItunes()
            .map {
                mapper.mapFromDB(it)
            }
    }
}
