package com.appetiser.itunes.data

import com.appetiser.module.data.poko.AccessToken
import com.appetiser.module.data.poko.UserSession
import io.reactivex.Completable
import io.reactivex.Single

interface AuthSource {

    fun login(email: String, password: String): Single<Map<String, Any>>

    fun saveCredentials(user: UserSession): Single<UserSession>

    fun saveToken(token: String): Single<AccessToken>

    fun getUserSession(): Single<UserSession>

    fun getUserToken(): Single<AccessToken>

    fun signup(
        email: String,
        password: String,
        passwordConfirmation: String,
        firstName: String,
        lastName: String,
        dob: String,
        country: String,
        mobileNumber: String,
        address: String
    ): Single<Map<String, Any>>

    fun forgotPassword(email: String): Single<Boolean>

    fun isUserLoggedIn(): Single<Boolean>

    fun logout(): Completable
}
