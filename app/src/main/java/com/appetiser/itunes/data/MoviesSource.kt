package com.appetiser.itunes.data

import com.appetiser.module.data.poko.Movie
import io.reactivex.Single

interface MoviesSource {

    fun getMovies(): Single<Map<String, Any>>

    fun saveMovies(itunes: List<Movie>): Single<List<Movie>>
}
