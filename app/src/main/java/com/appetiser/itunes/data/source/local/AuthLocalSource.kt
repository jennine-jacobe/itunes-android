package com.appetiser.itunes.data.source.local

import android.content.SharedPreferences
import androidx.room.Transaction
import com.appetiser.itunes.data.AuthSource
import com.appetiser.module.data.mapper.UserSessionMapper
import com.appetiser.module.data.poko.AccessToken
import com.appetiser.module.data.poko.UserSession
import com.appetiser.module.local.AppDatabase
import com.appetiser.module.local.model.DBToken
import com.appetiser.module.local.model.DBUserSession
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

class AuthLocalSource @Inject
constructor(private val sharedPreferences: SharedPreferences, private val database: AppDatabase, private val mapper: UserSessionMapper) : AuthSource {

    override fun login(email: String, password: String): Single<Map<String, Any>> = Single.just(mutableMapOf())

    override fun saveCredentials(user: UserSession): Single<UserSession> {
        return Single.create { emitter ->
            val dbUser = DBUserSession(fullName = user.fullName,
                    firstName = user.firstName,
                    lastName = user.lastName,
                    email = user.email,
                    photoUrl = user.photoUrl,
                    uid = user.uid,
                    dateOfBirth = user.dateOfBirth,
                    emailVerifiedAt = user.emailVerifiedAt)
            database.userSessionDao().insert(dbUser)
            emitter.onSuccess(user)
        }
    }

    override fun saveToken(token: String): Single<AccessToken> {
        return Single.create { emitter ->
            database.tokenDao().insert(DBToken(token = token))
            emitter.onSuccess(AccessToken(token))
        }
    }

    override fun getUserSession(): Single<UserSession> {
        return database.userSessionDao().getUserInfo()
                .map {
                    mapper.mapFromDB(it)
                }
                .onErrorReturnItem(UserSession())
    }

    override fun getUserToken(): Single<AccessToken> {
        return database.tokenDao().getToken()
                .map {
                    mapper.mapFromDB(it)
                }
                .onErrorReturnItem(AccessToken())
    }

    override fun signup(
        email: String,
        password: String,
        passwordConfirmation: String,
        firstName: String,
        lastName: String,
        dob: String,
        country: String,
        mobileNumber: String,
        address: String
    ): Single<Map<String, Any>> = Single.just(mapOf())

    override fun forgotPassword(email: String): Single<Boolean> = Single.just(false)

    @Transaction
    override fun isUserLoggedIn(): Single<Boolean> {
        return Single.zip(getUserSession(), getUserToken(), BiFunction { userSession, token ->
            if (userSession.uid.isEmpty() || token.token?.isEmpty()!!) {
                return@BiFunction false
            }

            return@BiFunction true
        })
    }

    @Transaction
    override fun logout(): Completable {
        return Completable.create {
            database.userSessionDao().logoutUser()
            database.tokenDao().logoutToken()
            it.onComplete()
        }
    }
}
