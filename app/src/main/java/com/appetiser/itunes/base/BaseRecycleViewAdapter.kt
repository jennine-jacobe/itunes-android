package com.appetiser.itunes.base

import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecycleViewAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mData = arrayListOf<Any>()

    /**
    * Adds all ot the elements of the collection to the end of the list
    */
    fun addAllData(dataFrom: List<Any>) {
        mData.addAll(dataFrom)
    }

    /**
     * Adds element to the end of the list
     */
    fun addData(dataFrom: Any) {
        mData.add(dataFrom)
    }

    /**
     * Adds element to the list on the position specified
     */
    fun addData(position: Int, dataFrom: Any) {
        mData.add(position, dataFrom)
    }

    /**
     * Removes list element at the specified position
     */
    fun removeDataAt(position: Int): Int {
        mData.removeAt(position)
        return position
    }

    /**
     * Removes all elements on the list
     */
    fun removeAllData() {
        mData.clear()
    }

    /**
     * Removes element that matches specified
     * */
    fun removeData(any: Any) {
        mData.remove(any)
    }

    fun getItemByPosition(position: Int): Any {
        return mData[position]
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    /**
     * Clear up data, this does not delete the first element if it is type
     * of [String]
     */
    fun clear() {
        if (mData.size > 0 && mData[0] is String) {
            mData.subList(1, mData.size).clear()
        }
    }

    fun clearFromPositionToEnd(position: Int) {
        if (position < mData.size) {
            mData.subList(position, mData.size).clear()
        }
    }
}
