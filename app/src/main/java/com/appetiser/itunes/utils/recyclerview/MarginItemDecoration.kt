package com.appetiser.itunes.utils.recyclerview

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Allows margin or spacings to be added on RecyclerView
 */
class MarginItemDecoration(private val spaceHeight: Float) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        with(outRect) {
            val height = spaceHeight.toInt()
            if (parent.getChildAdapterPosition(view) == 0) {
                top = height
            }
            left = height
            right = height
            bottom = height
        }
    }
}
