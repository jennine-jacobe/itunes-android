package com.appetiser.itunes.utils

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.itunes.ext.loadGlideWithPlaceholder
import com.appetiser.itunes.utils.recyclerview.MarginItemDecoration

object BindingAdapters {

    /**
     * Annotate @JvmStatic, BindingAdapter requires static method
     * */
    @JvmStatic
    @BindingAdapter("loadGlide")
    fun loadGlide(imageView: ImageView, url: String) {
        imageView.loadGlideWithPlaceholder(url)
    }

    @JvmStatic
    @BindingAdapter("animatedVisibility")
    fun setAnimatedVisibility(target: View, isVisible: Boolean) {
        target.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("dividerHeight")
    fun setDividerHeight(view: RecyclerView, height: Float) {
        view.addItemDecoration(MarginItemDecoration(height))
    }

    @JvmStatic
    @BindingAdapter("adapter")
    fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
        view.adapter = adapter
    }
}
