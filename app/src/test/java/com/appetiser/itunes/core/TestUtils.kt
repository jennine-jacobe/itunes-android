package com.appetiser.itunes.core

import com.appetiser.module.data.poko.UserSession

class TestUtils {

    companion object {
        val email = "foo@bar.baz"
        val password = "password"
        val passwordConfirmation = "password"
        val firstName = "foo"
        val lastName = "bar"
        val dob = "01/01/2000"
        val country = "Philippines"
        val mobileNumber = "09123456789"
        val address = "Cebu City"

        val userSession = UserSession(
                fullName = "Test",
                firstName = "Test",
                lastName = "Test",
                email = "test@test.test",
                photoUrl = "http://photo",
                uid = "1",
                dateOfBirth = "1984/11/11",
                emailVerifiedAt = "2019/11/11")
    }
}
